const donnees = {}

const donneesEntrante = 
`
[
	{
		"question":"À quoi sert un aria-label?",
		"reponses":[
		"Ajouter du contenu textuel sur une balise pour aider les lecteurs d'écran",
		"À rien", 
		"Je ne sais pas"
		], 
		"reponse":0
	},
	{
		"question":"HTML vient de :",
		"reponses":[
			"Hyper Typo Meta Lol",
			"Hypertext markup language", 
			"Je ne sais pas"
		], 
		"reponse":1
	}
]

`

$(function () {
  AjouterValidations()
  $('#myTable').DataTable(
    {
      "language":
      {
          "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/French.json",
          "sSearchPlaceholder": "recherche..."
      },
  }
  );
})

function AjouterValidations() {
  $("form[name='questionnaire']").validate(
    {
      rules: {
        prenom: "required",
        nom: "required",
        date_naissance: {
          regex: /^[1-9]/,
          required: true
        }
      },
      messages: {
        prenom: "Veuillez entrer un prénom",
        nom: "Veuillez entrer un nom",
        date_naissance: {
          required: 'La date de naissance est requise'
        }
      },
      // getAge: function getAge (date_naissance) {
      //   const dateDAjourdhui = new Date()
      //   const dateNaissance = new Date(date_naissance.dateNaissance)
      //   let age = dateDAjourdhui.getFullYear() - dateNaissance.getFullYear()
      //   const m = dateDAjourdhui.getMonth() - dateNaissance.getMonth()
      //   if (m < 0 || (m === 0 && dateDAjourdhui.getDate() < dateNaissance.getDate())) {
      //     age--
      //   }
      //   return age
      // },
      submitHandler: function (form) { // CE QUE LE FORMULAIRE FAIRE UNE FOIS SOUMIS
        donnees.prenom = $('#prenom').val()
        donnees.nom = $('#nom').val()
        // creerQuiz()
        $('#questionnaire').remove()
        // creerQuiz()
      },
      showErrors: function (errorMap, errorList) {
        if (est_soumis) {
          var sommaire = "Vous avez les erreurs: \n";
          const ul = $('<ul></ul>')
          $.each(errorList, function () { ul.append(`<li>${this.message}</li>`) })
          $('#sommaire-erreurs').html(ul)
          est_soumis = false;
        }
        this.defaultShowErrors();
      },
      invalidHandler: function (form, validator) {
        est_soumis = true;
      }
    });
    //   function creerQuiz(){
    //     const donneesQuestion = JSON.parse(donneesEntrante)
    //     const question = $('<h1>' + donneesQuestion[0].question + '</h1>')
    //     $('main').append(question)

    //     for (let i = 0; i > donneesQuestion[0].reponse.length; i++ ){
    //       const reponse = $('<input>' + donneesQuestion[0].reponses[i] + '</input>')
    //       reponse.attr('type','radio')
    //     }
    //     $('main').append(reponse)
    // }
}

function creerQuiz(){
  const donneesQuestion = JSON.parse(donneesEntrante)
  const question = $('<h1>' + donneesQuestion[0].question + '</h1>')
  $('main').append(question)

  for (let i = 0; i > donneesQuestion[0].reponse.length; i++ ){
    const reponse = $('<input>' + donneesQuestion[0].reponses[i] + '</input>')
    reponse.attr('type','radio')
  }
  $('main').append(reponse)
}

//https://www.pierrefay.fr/blog/jquery-validate-formulaire-validation-tutoriel.html
$.validator.addMethod(
  "regex",
  function (value, element, regexp) {
    if (regexp.constructor != RegExp)
      regexp = new RegExp(regexp);
    else if (regexp.global)
      regexp.lastIndex = 0;
    return this.optional(element) || regexp.test(value);
  }, "erreur expression reguliere"
);

jQuery.validator.addMethod("alphanumeric", function (value, element) {
  return this.optional(element) || /^[\w.]+$/i.test(value);
}, "Letters, numbers, and underscores only please");

// const donneesEntrantes = `
// [{
//     "question":"question1",
//     "reponses":["allo", "toi", "pizza"],
//     "bonne" : 0, 
//     "reponseFournie":1
// },{
//     "question":"question2",
//     "reponses":["allo2", "toi2", "pizza2"],
//     "bonne" : 1, 
//     "reponseFournie":1
// }]
// `
//     const donnees = JSON.parse(donneesEntrantes)
//     $(document).ready(function () {
//       creerTable(donnees)
//     });
 
//     function creerTable(donnees) {
//       for (let i = 0; i < donnees.length; i++) {
//         const questionCourante = donnees[i]
//         const tr = $('<tr></tr>')
//         tr.append('<td>' + (i + 1) + '</td>')  // le +1 est pour ne pas avoir de question #0 et les parenthèses sont obligatoires.
//         tr.append('<td>' + questionCourante.question + '</td>')
//         if (questionCourante.bonne === questionCourante.reponseFournie ) {
//           tr.append('<td>' + 'crochet' + '</td>') // mettre des styles et un vrai crochet
//         } else {
//           tr.append('<td>X</td>') // mettre des styles
//         }
//         $('tbody').append(tr)
//       }
//     }